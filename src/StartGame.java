import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Random;
import java.util.Scanner;

import static java.lang.Thread.sleep;

public class StartGame implements Runnable {
    private int CELL_SIZE = 45;
    private int NR_CELLS = 9;
    private JPanel panel;
    private JLabel scoreLabel;
    private JLabel high_scoreLabel;
    private JLabel timeLabel;
    private Random rand = new Random();

    private Frame frame;
    private CardLayout layout;
    private JPanel cardPanel;
    //JPanel panelRankList;

    private JPanel panelGameOver = new JPanel();
    private JTextField gamerName = new JTextField("");
    private JButton gamerNameWritten = new JButton("Done");
    private JTextArea lose = new JTextArea("Game over!\n Please enter your name.");

    private JPanel panel1 = new JPanel();

    private int[] fields;
    private int[] nextColors = new int[3];
    private  int[] nextFields = new int[3];

    private int init_score;                 // paraméterként kapja az osztály, és ezekkel inicializálja az Update és TimeMeasure osztályokat
    private int init_high_score;
    private int init_hours;
    private int init_minutes;
    private int init_seconds;
    private boolean init_newHighScore;

    private int empty_cells_left;

    private boolean clicked = false;                // igaz, ha ki van jelölve egy mező
    private int clickedField;

    private boolean ballDisappeared;
    private boolean gameOver=false;

    private boolean soundON;
    private Thread thread;

    private Update update;
    private TimeMeasure timeMeasure;

    public StartGame(JPanel panel, JLabel score, JLabel high_score, JLabel time, int sc, int high_sc,
                          int hours, int minutes, int seconds, boolean newHighScore, int ecl, Frame frame, CardLayout layout, JPanel cardPanel) {
        this.panel=panel;
        this.scoreLabel=score;
        this.high_scoreLabel=high_score;
        this.timeLabel=time;
        this.init_score=sc;
        this.init_high_score=high_sc;
        this.init_hours=hours;
        this.init_minutes=minutes;
        this.init_seconds=seconds;
        this.init_newHighScore=newHighScore;
        this.empty_cells_left=ecl;
        this.frame=frame;
        this.layout=layout;
        this.cardPanel=cardPanel;

        soundON=true;

        panelGameOver.setLayout(new GridLayout(2,1));
        panelGameOver.add(lose);
        lose.setEditable(false);

        panel1.setLayout(new GridLayout(1,2));
        panel1.add(gamerName);
        panel1.add(gamerNameWritten);

        panelGameOver.add(panel1);

        cardPanel.add(panelGameOver,"panelGameOver");

        fields = new int[NR_CELLS*NR_CELLS];
        for(int i=0;i<NR_CELLS*NR_CELLS;i++){               // kezdetben üres a tábla
            fields[i]=0;
        }

        if(empty_cells_left==NR_CELLS*NR_CELLS && sc==0){
            nextColors[0]=nextColors[1]=nextColors[2]=0;
            nextFields[0]=nextFields[1]=nextFields[2]=0;
            generateNewBalls(5);                        // ha új játékot indítottunk, akkore kigenerálja a kezdeti labdákat
            empty_cells_left-=5;
            generateNextBalls(3);
        }
    }

    @Override
    public void run(){
        update = new Update(scoreLabel, high_scoreLabel, init_score, init_high_score, init_newHighScore);
        timeMeasure = new TimeMeasure(timeLabel,init_hours,init_minutes,init_seconds);

        panel.repaint();

        panel.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {                // csak akkor történik valami, ha kattintttunk
                super.mouseClicked(e);

                ballDisappeared=false;

                if(timeMeasure.getInGame()) {
                    int x = e.getX();
                    int y = e.getY();
                    if (y >= 15 + CELL_SIZE && y < NR_CELLS * CELL_SIZE + CELL_SIZE + 15) { //ha nem a táblára kattintunk, nem történik semmi
                        int fieldNumber = (y - 15 - CELL_SIZE) / CELL_SIZE * NR_CELLS + x / CELL_SIZE;
                        if (fields[fieldNumber] > 0 && fields[fieldNumber] < 8) {           // ha már a táblán lévő golyóra kattintottunk
                            if (clicked) {
                                clicked = false;
                                fields[clickedField] -= 14;                     // ha már előzőleg ki volt jelölve egy golyó, azt érvényteleníti
                            }
                            clicked = true;
                            clickedField = fieldNumber;
                            fields[fieldNumber] += 14;
                            panel.repaint();
                        } else {                                                                // ha nem golyóra kattintottunk
                            if (clicked) {
                                if (fields[fieldNumber] >= 15) {                 // mivel egyszerre csak egy mező lehet "clicked" állapotban
                                    clicked = false;                                // az biztos, hogy az előzőleg kijelölt mező.
                                    fields[clickedField] -= 14;                 // ekkor évvényteleníti a kijelölést
                                    panel.repaint();
                                } else {                                         // ha a golyót üres helyre szeretnénk áthelyezni
                                    int[] path;
                                    FindPath fp = new FindPath(clickedField, fieldNumber, fields);
                                    int k = fp.getEl_lehet_jutni();             // megkeresem a labda útját a kiválasztott pozícióhoz
                                    if (k >= 0) {
                                        path = fp.getEredmeny();
                                        /*if(soundON){
                                            thread=new Thread(new PlaySound("MoveBallSound.wav",false));
                                            thread.start();
                                        }*/
                                        Thread t = new Thread(new MoveBall(path, k, clickedField, fields, panel));
                                        t.start();
                                        try {
                                            t.join();
                                        } catch (InterruptedException ex) {
                                            ex.printStackTrace();
                                        }
                                        panel.repaint();
                                        try {
                                            sleep(100);
                                        }catch (InterruptedException ex){
                                            ex.printStackTrace();
                                        }
                                        clicked = false;

                                        int points = getPoints(fieldNumber);            // megnézi, hogy kigyűlt-e legalább 5 golyó egymás után

                                        if (points == 0) {           // ha nem gyűlt, akkor elhelyezi az előzőleg kigenerált golyókat
                                            if (empty_cells_left <= 3) {
                                                placeBalls(empty_cells_left);
                                            } else {
                                                placeBalls(3);      // elhelyezi az előzőleg kigenerált golyókat
                                            }
                                            for(int i=0;i<3;i++) {
                                                int nxtFieldNr=nextFields[i];
                                                nextFields[i]=-1;                   // mivel ezeket már elhelyezte, megváltoztathatom,
                                                points = getPoints(nxtFieldNr);     // hogy ne legyenek gondok a pontszámolásnál
                                                if(points>0) {
                                                    if(soundON) {
                                                        try {
                                                            //thread.join();
                                                        } catch (Exception ex){
                                                            ex.printStackTrace();
                                                        }
                                                        ballDisappeared=true;
                                                        thread = new Thread(new PlaySound("BallDisappearSound.wav", false));
                                                        thread.start();
                                                    }
                                                    update.setScore(points);
                                                } else{
                                                    //thread=new Thread(new PlaySound("MoveBallSound.wav",false));
                                                    //thread.start();
                                                }
                                            }
                                            panel.repaint();
                                            try {
                                                sleep(100);
                                            }catch (InterruptedException ex){
                                                ex.printStackTrace();
                                            }
                                            if (empty_cells_left >=3) {     // ha még van hely a táblán
                                                generateNextBalls(3);
                                                panel.repaint();
                                            } else {                        // ha betelt a tábla
                                                generateNextBalls(empty_cells_left);
                                                placeBalls(empty_cells_left);
                                                panel.repaint();
                                                empty_cells_left=0;
                                                panel.repaint();
                                                gameOver=true;
                                                EndGame();
                                            }
                                        } else {                // ha kaptunk pontot, akkor nem történik semmi, csak pontot kapunk
                                            if(soundON) {
                                                ballDisappeared=true;
                                                thread = new Thread(new PlaySound("BallDisappearSound.wav", false));
                                                thread.start();
                                            }
                                            update.setScore(points);
                                        }
                                        if(!ballDisappeared && !gameOver && soundON) {
                                            thread = new Thread(new PlaySound("MoveBallSound.wav", false));
                                            thread.start();
                                        }
                                    } else {                // ha nincs a kijelölt helyhez vezető szabad út
                                        if(soundON){
                                            thread = new Thread(new PlaySound("NoPathSound.wav",false));
                                            thread.start();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });

        Thread thTimeMeasure = new Thread(timeMeasure);         // elindítja az időmérést
        thTimeMeasure.start();
    }

    public void EndGame(){
        timeMeasure.setInGame(false);                   // leállítja az időmérést
        layout.show(cardPanel,"panelGameOver");

        if(update.getNewHighScore()){
            if(soundON){
                thread = new Thread(new PlaySound("NewHighScoreSound.wav",false));
                thread.start();
            }
            lose.setText(lose.getText() + "\nNEW HIGH SCORE!");
        } else{
            if(soundON) {
                thread = new Thread(new PlaySound("GameOverSound.wav", false));
                thread.start();
            }
        }

        int[] rankListScores = new int[11];
        char[][] rankListNames = new char[11][];

        gamerNameWritten.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int nr_scores=0;
                try {
                    String data;
                    Scanner scanner = new Scanner(new FileReader("RankList/rankList.txt"));
                    int i=0;
                    while (scanner.hasNextLine() && i<10) {                 // beolvassa a ranglistát, hozzáadja a jelenlegi pontszámot, majd visszaírja
                        data = scanner.nextLine();
                        rankListNames[i]=data.toCharArray();

                        data=scanner.nextLine();
                        rankListScores[i]=Integer.parseInt(data);

                        i++;
                    }
                    scanner.close();
                    nr_scores=i;
                } catch (Exception exception){
                    exception.printStackTrace();
                }
                String name = gamerName.getText();
                rankListNames[nr_scores]=name.toCharArray();
                rankListScores[nr_scores]=update.getScore();

                System.out.println(nr_scores);
                BubbleSortScores(rankListScores,rankListNames, nr_scores);              // csökkenő sorrendbe helyezi a pontszámokat
                try{
                    FileWriter fw = new FileWriter("RankList/rankList.txt");

                    for(int i=0;i<nr_scores;i++){
                        fw.write(String.valueOf(rankListNames[i]) + "\n");
                        fw.write(rankListScores[i] + "\n");
                    }
                    fw.close();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
                layout.show(cardPanel,"panelOptions");
            }
        });
    }

    public void BubbleSortScores(int[] scores, char[][] names, int n){              // buborékos rendezés
            boolean ok;
            int k, sz, x;
            char[] y;
            k=n;
            do{
                sz=k-1;
                ok=true;
                for(int j=0;j<=sz;j++){
                    if(scores[j]<scores[j+1]){
                        ok=false;
                        x=scores[j];
                        scores[j]=scores[j+1];
                        scores[j+1]=x;
                        y=names[j];
                        names[j]=names[j+1];
                        names[j+1]=y;
                        k=j;
                    }
                }
            }
            while (!ok);
    }

    public void placeBalls(int nr){         // elhelyezi a nextFields és nextColors tömbök szernt a következő nr darab golyót a táblán
        for(int i=0;i<nr;i++){

            if(fields[nextFields[i]]<=7){               // ha egy másik golyó elfoglalta a helyét,
                generateNextBalls(i,nextColors[i]);     // akkor új helyet kell találni neki
            }
            fields[nextFields[i]]=nextColors[i];
            empty_cells_left-=1;

        }
    }

    public void deleteBalls(int[] balls, int nr_balls){         // kitörli azokat a golyókat a tábláról, amelyeket paraméterként megkap egy tömbben
        for(int i=0;i<nr_balls;i++){
            if(fields[balls[i]]!=0) {
                fields[balls[i]] = 0;
                for (int j = 0; j < 3; j++) {
                    if (balls[i] == nextFields[j]) {
                        fields[balls[i]] = nextColors[j] + 7;
                    }
                }
                empty_cells_left+=1;
            }
        }
    }

    public  int getPoints(int fieldNumber){                 // megnézi, hogy a kapott golyó lerakása után kigyűlt-e legalább 5 egymás után
        int vertical=1, horizontal=1, main_diagonal=1, minor_diagonal=1;
        int[] balls_vertical=new int[NR_CELLS];
        int[] balls_horizontal=new int[NR_CELLS];
        int[] balls_main_diagonal=new int[NR_CELLS];
        int[] balls_minor_diagonal=new int[NR_CELLS];

        balls_vertical[0]=balls_horizontal[0]=balls_main_diagonal[0]=balls_minor_diagonal[0]=fieldNumber;

        int color=fields[fieldNumber];

        int cellNr=fieldNumber-NR_CELLS;                        // függőleges felfele
        while (cellNr>=0 && fields[cellNr]==color){
            balls_horizontal[horizontal]=cellNr;
            horizontal++;
            cellNr-=NR_CELLS;
        }
        cellNr=fieldNumber+NR_CELLS;                            // függőleges lefele
        while (cellNr>=0 && cellNr<NR_CELLS*NR_CELLS && fields[cellNr]==color){
            balls_horizontal[horizontal]=cellNr;
            horizontal++;
            cellNr+=NR_CELLS;
        }

        cellNr=fieldNumber-1;                                   // vízszintes bal
        while (cellNr>=0 && cellNr%NR_CELLS<NR_CELLS-1 && fields[cellNr]==color){
            balls_vertical[vertical]=cellNr;
            vertical++;
            cellNr-=1;
        }
        cellNr=fieldNumber+1;                                   // vízszintes jobb
        while (cellNr>=0 && cellNr%NR_CELLS>0 && fields[cellNr]==color){
            balls_vertical[vertical]=cellNr;
            vertical++;
            cellNr+=1;
        }

        cellNr=fieldNumber-NR_CELLS-1;                                   // főátló fel-balra
        while (cellNr%NR_CELLS<NR_CELLS-1 && cellNr>=0 && fields[cellNr]==color){
            balls_main_diagonal[main_diagonal]=cellNr;
            main_diagonal++;
            cellNr-=NR_CELLS+1;
        }
        cellNr=fieldNumber+NR_CELLS+1;                                   // főátló le-jobbra
        while (cellNr>=0 && cellNr%NR_CELLS>0 && cellNr<NR_CELLS*NR_CELLS && fields[cellNr]==color){
            balls_main_diagonal[main_diagonal]=cellNr;
            main_diagonal++;
            cellNr+=NR_CELLS+1;
        }

        cellNr=fieldNumber-NR_CELLS+1;                                   // mellékátló fel-jobbra
        while (cellNr%NR_CELLS>0 && cellNr>=0 && fields[cellNr]==color){
            balls_minor_diagonal[minor_diagonal]=cellNr;
            minor_diagonal++;
            cellNr+=-NR_CELLS+1;
        }
        cellNr=fieldNumber+NR_CELLS-1;                                   // mellékátló le-balra
        while (cellNr>=0 && cellNr%NR_CELLS<NR_CELLS-1 && cellNr<NR_CELLS*NR_CELLS && fields[cellNr]==color){
            balls_minor_diagonal[minor_diagonal]=cellNr;
            minor_diagonal++;
            cellNr+=NR_CELLS-1;
        }

        int sum=0;
        if(vertical>=5) {
            sum += vertical-1;                      // a fieldNumber indexű golyót nem számolom bele, csak a legvégén
            deleteBalls(balls_vertical, vertical);
        }
        if(horizontal>=5) {
            sum += horizontal-1;
            deleteBalls(balls_horizontal, horizontal);
        }
        if(main_diagonal>=5) {
            sum += main_diagonal-1;
            deleteBalls(balls_main_diagonal, main_diagonal);
        }
        if(minor_diagonal>=5){
            sum+=minor_diagonal-1;
            deleteBalls(balls_minor_diagonal,minor_diagonal);
        }
        if(sum>0) {
            sum += 1;             // hozzáadom a fieldNumber indexű golyót is a darabszámhoz
        }
        return sum*sum;         // visszatéríti a kapott pontszámot
    }

    public void generateNextBalls(int nr){              // kienerálja, hogy hova fog érkezni a következő nr golyó, de nem helyezi el
        int randCell;
        int randColor;
        for(int k=0;k<nr;k++) {
            nextColors[k] = rand.nextInt(7) + 1;
            randCell = rand.nextInt(empty_cells_left-k);
            randColor = nextColors[k] + 7;
            int i = 0, j = -1;
            while (i <= randCell) {
                j++;
                if (fields[j] == 0) {
                    i++;
                }
            }
            fields[j] = randColor;
            nextFields[k]=j;
        }
    }

    public void generateNextBalls(int index, int color) {       // kienerálja, hogy hova fog érkezni a következő nr darab, adott színű golyót, de nem helyezi el
        int randCell;
        randCell = rand.nextInt(empty_cells_left-3+index+1);
        int i = 0, j = -1;
        while (i <= randCell) {
            j++;
            if (fields[j] == 0) {
                i++;
            }
        }
        fields[j] = color;
        nextFields[index] = j;
    }

    public void generateNewBalls(int nr){           // egyből el is helyezi a golyókat
        int randCell;
        int randColor;
        for(int k=0;k<nr;k++){
            randCell=rand.nextInt(empty_cells_left-k);
            randColor=rand.nextInt(7)+1;
            int i=0,j=-1;
            while(i<=randCell){
                j++;
                if(fields[j]==0){
                    i++;
                }
            }
            fields[j]=randColor;
        }
    }

    public Update getUpdate(){
        return update;
    }

    public TimeMeasure getTimeMeasure(){
        return timeMeasure;
    }

    public int[] getNextColors(){
        return nextColors;
    }

    public int[] getFields(){
        return  fields;
    }

    public void setFields(int[] fields){
        this.fields=fields;
    }

    public int getEmpty_cells_left(){
        return empty_cells_left;
    }

    public int[] getNextFields() { return nextFields; }

    public void setNextColors(int[] nextColors){
        this.nextColors=nextColors;
    }

    public void setNextFields(int[] nextFields){
        this.nextFields=nextFields;
    }

    public void setSoundON(boolean soundON){
        this.soundON=soundON;
    }
}
