import javax.sound.sampled.*;
import java.io.File;

public class PlaySound implements Runnable {
    private boolean loop;

    private File audioFile;
    private AudioInputStream audioInputStream;
    private AudioFormat format;
    private DataLine.Info info;
    private Clip audioClip;

    public PlaySound(String filename, boolean loop){
        this.loop=loop;

        try {
            audioFile = new File("Sounds/" + filename);
            audioInputStream = AudioSystem.getAudioInputStream(audioFile);
            format = audioInputStream.getFormat();
            info = new DataLine.Info(Clip.class, format);
            audioClip = (Clip) AudioSystem.getLine(info);
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        try {
            audioClip.open(audioInputStream);
            if(loop) {
                audioClip.loop(Clip.LOOP_CONTINUOUSLY);
            }
            audioClip.start();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void StopSound(){
        audioClip.stop();
        audioClip.close();
    }

    /*public void setVolume(double gain){
       // FloatControl volumeControl = SoundTools.getControl(file);
        try {
            FloatControl gainControl = (FloatControl) audioClip.getControl(FloatControl.Type.VOLUME);
            float dB = (float) (Math.log(gain) / Math.log(10.0) * 20.0);
            gainControl.setValue(dB);
        } catch (Exception e){
            e.printStackTrace();
        }
    }*/
}
