import javax.swing.*;
import java.text.DecimalFormat;

public class TimeMeasure implements Runnable {
    private int hours;
    private int minutes;
    private int seconds;
    private JLabel time;
    boolean inGame;

    public TimeMeasure(JLabel time,int hours, int minutes, int seconds){
        inGame=true;
        this.time=time;
        this.hours=hours;
        this.minutes=minutes;
        this.seconds=seconds;
    }

    @Override
    public void run() {             // folyamatosan méri az időt
        while(inGame){
            if(hours==0) {
                time.setText("Time: " + new DecimalFormat("00").format(minutes) + ":" + new DecimalFormat("00").format(seconds));
            }
            else{
                time.setText("Time: " + new DecimalFormat("00").format(hours) + ":"
                        + new DecimalFormat("00").format(minutes) + ":" + new DecimalFormat("00").format(seconds));
            }
            try{
                Thread.sleep(1000);
            }
            catch (InterruptedException|IllegalArgumentException e){
                e.printStackTrace();
            }
            seconds++;
            if(seconds==60){
                seconds=0;
                minutes++;
                if(minutes==60){
                    minutes=0;
                    hours++;
                }
            }
        }
    }

    public void setInGame(boolean inGame) {
        this.inGame = inGame;
    }

    public boolean getInGame(){
        return inGame;
    }

    public int getHours(){
        return hours;
    }
    public int getMinutes(){
        return minutes;
    }

    public int getSeconds(){
        return seconds;
    }
}
