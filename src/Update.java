import javax.swing.*;

public class Update {
    private int highScore;
    private boolean newHighScore;
    private int score;

    private JLabel scoreLabel;
    private JLabel high_scoreLabel;

    public Update(JLabel scoreLabel, JLabel high_scoreLabel, int score, int highScore, boolean newHighScore){
        this.score=score;
        this.highScore=highScore;
        this.newHighScore=newHighScore;
        this.scoreLabel=scoreLabel;
        this.high_scoreLabel=high_scoreLabel;
        this.newHighScore=newHighScore;

        if(score>highScore) {
            this.highScore = score;
            this.newHighScore = true;
        } else {
            this.newHighScore=false;
        }

        scoreLabel.setText("Score: " + score);
        high_scoreLabel.setText("High score: " + highScore);
    }
    public void setScore(int points){           // számolja a pontszámokat, és frissíti őket az ablakon
        score+=points;
        scoreLabel.setText("Score: " + score);
        if(score>highScore){
            highScore=score;
            high_scoreLabel.setText("High score: " + highScore);
            newHighScore=true;
        }
    }

    public int getScore(){
        return score;
    }

    public int getHigh_score(){
        return highScore;
    }

    public boolean getNewHighScore(){
        return newHighScore;
    }
}
