import javax.swing.*;

public class MoveBall implements Runnable {
    private int[] path;
    private int k;
    private int clickedField;
    private int fields[];
    private JPanel panel;

    public MoveBall(int[] path, int k, int clickedField, int[] fields, JPanel panel) {
        this.path = path;
        this.k = k;
        this.clickedField = clickedField;
        this.fields = fields;
        this.panel = panel;
    }

    public void run() {             // végigmegy a tömbön és sorban elhelyezi a labdákat az útvonal minden mezőjén
                int color = fields[clickedField] - 14;
                int seged = 0;
                for (int i = 1;i <= k; i++) {
                    fields[path[i - 1]] = seged;
                    seged = fields[path[i]];
                    fields[path[i]] = color;
                    panel.repaint();
                    try {
                        //Thread.sleep(100);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
    }
