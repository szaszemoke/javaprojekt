import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Scanner;

public class WINLines extends JPanel{
    private JFrame frame = new JFrame();
    private JPanel panel1 = new JPanel();
    private JPanel panelOptions = new JPanel();
    private JPanel panelGame = new JPanel();
    private JPanel panelHelp = new JPanel();
    private JPanel panelRankList = new JPanel();
    private JMenu jMenu = new JMenu("Menu");
    private JMenuBar jMenuBar = new JMenuBar();
    private JMenuItem options = new JMenuItem("Options");

    //JButton undo = new JButton("Undo");
    private JButton continueCurrentButton = new JButton("Continue current game");
    private JButton helpButton = new JButton("Help");
    private JButton newGameButton = new JButton("New game");
    private JButton saveButton = new JButton("Save game");
    private JButton continueButton = new JButton("Continue saved game");
    private JButton rankListButton = new JButton("Ranking list");
    private JButton soundButton = new JButton("Sound off");
    private JButton musicButton = new JButton("Music off");

    private JTextArea rankList = new JTextArea();

    private JTextArea helpText = new JTextArea();
    private JScrollPane scrollPane;                             // a helpText görgetéséhez
    private JFileChooser jFileChooser = new JFileChooser("./SavedGames");

    private int CELL_SIZE = 45;                       // a mezők (négyzetek) szélessége és hosszúsága
    private int NR_CELLS = 9;                         // egy sorban/oszlopban levő mezők száma
    private int BOARD_SIZE = (CELL_SIZE+2)*(NR_CELLS);
    private JLabel score=new JLabel("Score: 0");
    private JLabel high_score = new JLabel("High score: 0");
    private JLabel time = new JLabel("Time:");

    private BufferedImage[] img;

    private boolean wasInGame=false;

    private boolean soundON=true;

    private PlaySound playSound;
    private StartGame startGame;
    private Thread thread;
    private Thread soundThread;

    public WINLines(){
        Initialize();
    }

    public void Initialize(){
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setTitle("WINLines");
        CardLayout layout = new CardLayout();
        JPanel cardPanel = new JPanel();
        cardPanel.setLayout(layout);
        frame.add(cardPanel);

        panel1.setLayout(new GridLayout(1,3));
        frame.setBounds(0,0,BOARD_SIZE,BOARD_SIZE+120);

        img=new BufferedImage[22];

        String[] img_names = new String[]{"background", "red", "darkred", "yellow", "green", "lightblue", "blue", "pink"};

        try {
            for(int i=0;i<8;i++) {
                img[i] = ImageIO.read(new File("Pictures/" + img_names[i] + "45.png"));
            }
            for(int i=1;i<8;i++){
                img[i+7] = ImageIO.read(new File("Pictures/" + img_names[i] + "_mini45.png"));
            }
            for(int i=1;i<8;i++){
                img[i+14] = ImageIO.read(new File("Pictures/" + img_names[i] + "_clicked45.png"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        panel1.add(score);                                      // a játék közben végig látszik az eltelt idő, a jelenlegi- és az eddig elért
        panel1.add(high_score);                                 // legmagasabb pontszám
        panel1.add(time);

        score.setBounds(0,0,30,15);
        high_score.setBounds(30,0,30,15);

        panelGame.setLayout(new BorderLayout());
        panelGame.add(panel1,BorderLayout.SOUTH);
        panelGame.add(this,BorderLayout.CENTER);        // a saját panelre lesz kirajzolva a játék tábla

        /*panelGame.add(undo);

        undo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });*/

        jMenu.add(options);
        jMenuBar.add(jMenu);
        frame.setJMenuBar(jMenuBar);

        options.addActionListener(e -> {
            layout.show(cardPanel, "panelOptions");
            if (wasInGame) {
                startGame.getTimeMeasure().setInGame(false);                            // amikor a menüben vagyunk, már nem kell mérni az időt
            }
        });

        panelOptions.setLayout(new GridLayout(7,1));

        //panelOptions.add(continueCurrentButton);
        //continueCurrentButton.setVisible(false);
        panelOptions.add(newGameButton);
        panelOptions.add(saveButton);
        panelOptions.add(continueButton);
        panelOptions.add(musicButton);
        panelOptions.add(soundButton);
        panelOptions.add(rankListButton);
        panelOptions.add(helpButton);

        helpText.setText("Instructions:\nLines 98 classic game interface is a square of 9 horizontal lines,\n 9 vertical lines, creating 81 small squares,\n each ball will have different colors(7 colors). Mission Your arrangement is to have at least 5 balls of the same color lying on a horizontal, vertical or diagonal line to score. After each move will automatically generate 3 new balls with random colors and positions. You need to calculate and skillfully arrange for the number of balls disappear larger than the number of balls generated, otherwise they will occupy all 81 squares and the game will be over! It sounds simple but in reality it is not simple at all, you need to play a lot of times to accumulate more experience, as well as cleverly calculate your moves.");
        helpText.setEditable(false);
        scrollPane = new JScrollPane(helpText);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        //scrollPane.setSize(BOARD_SIZE-40,BOARD_SIZE);
        helpText.setSize(BOARD_SIZE-50,BOARD_SIZE);
        panelHelp.add(scrollPane);
        helpText.setLineWrap(true);                                     // sortörés
        helpText.setWrapStyleWord(true);

        cardPanel.add(panelGame,"panelGame");
        cardPanel.add(panelOptions,"panelOptions");
        cardPanel.add(panelHelp,"panelHelp");
        cardPanel.add(panelRankList,"panelRankList");

        layout.show(cardPanel,"panelOptions");

        frame.setVisible(true);
        frame.setResizable(false);

        musicButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(musicButton.getText().equals("Music on")){
                    musicButton.setText("Music off");
                    playSound = new PlaySound("BackgroundMusic.wav",true);
                    soundThread = new Thread(playSound);
                    soundThread.start();


                } else{
                    musicButton.setText("Music on");
                    playSound.StopSound();
                }
            }
        });

        soundButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!soundON){
                    soundButton.setText("Sound off");
                    soundON=true;
                    startGame.setSoundON(true);
                } else{
                    soundButton.setText("Sound on");
                    soundON=false;
                    startGame.setSoundON(false);
                }
            }
        });

        helpButton.addActionListener(e -> layout.show(cardPanel,"panelHelp"));

        newGameButton.addActionListener(e -> {
            if(!wasInGame) {
                wasInGame = true;
                panelOptions.setLayout(new GridLayout(8, 1));
                panelOptions.add(continueCurrentButton, 0);
            }

            layout.show(cardPanel, "panelGame");
            //repaint();
            try {
                Scanner scanner = new Scanner(new FileReader("RankList/rankList.txt"));
                String data = scanner.nextLine();
                data = scanner.nextLine();                              // a ranglista első eleme a valaha elért legnagybb pontszámot tartalmazza
                int highScore = Integer.parseInt(data);
                scanner.close();
                startGame = new StartGame(WINLines.this, score,high_score,time,0, highScore, 0, 0, 0,
                        false, NR_CELLS*NR_CELLS, frame, layout, cardPanel);
                thread=new Thread(startGame);                           // külön szálban elindítom az új játékot
                thread.start();

                startGame.setSoundON(soundON);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        saveButton.addActionListener(e -> {
            jFileChooser.showSaveDialog(frame);
            try {
                FileWriter fw = new FileWriter(jFileChooser.getSelectedFile());                 // a megadott nevű állományba lementem
                                                                                                // az összes fontos információt:
                int[] fields = startGame.getFields();
                for (int i=0;i<NR_CELLS*NR_CELLS;i++){                      // a tábla tartalmát
                    fw.write(fields[i] + "\n");
                }
                int ecl=startGame.getEmpty_cells_left();                    // az üres mezők számát
                fw.write(ecl + "\n");

                int[] nextColors=startGame.getNextColors();                 // a kigenerált labdák helyét és színét
                for(int i=0;i<3;i++){
                    fw.write(nextColors[i] + "\n");
                }
                int[] nextFields=startGame.getNextFields();
                for(int i=0;i<3;i++){
                    fw.write(nextFields[i] + "\n");
                }

                Update update=startGame.getUpdate();

                fw.write(update.getScore() + "\n");                     // a jelenlegi pontszámot

                //fw.write(update.getHigh_score() + "\n");                // a legnagyobb pontszámot

                if(update.getNewHighScore()){                               // azt, hogy elért-e a játékos új rekordot
                    fw.write("1\n");
                }else{
                    fw.write("0\n");
                }

                TimeMeasure timeMeasure = startGame.getTimeMeasure();

                fw.write(timeMeasure.getHours() + "\n");                // a játékban eltelt időt

                fw.write(timeMeasure.getMinutes() + "\n");

                fw.write(timeMeasure.getSeconds() + "\n");

                fw.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        continueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!wasInGame) {
                    wasInGame = true;
                    panelOptions.setLayout(new GridLayout(8, 1));
                    panelOptions.add(continueCurrentButton, 0);
                }
                jFileChooser.showOpenDialog(frame);
                try{
                    Scanner scanner = new Scanner(new FileReader(jFileChooser.getSelectedFile()));
                    String data;
                    int[] fields = new int[NR_CELLS*NR_CELLS];                  // beolvasom a lementett adatokat a kiválasztott állományból
                    int ecl, sc, highScore, hours, minutes, seconds;
                    boolean newHighScore;

                    for(int i=0;i<NR_CELLS*NR_CELLS;i++){
                        data = scanner.nextLine();
                        fields[i]=Integer.parseInt(data);
                    }

                    data = scanner.nextLine();
                    ecl=Integer.parseInt(data);

                    int[] nextColors = new int[3];
                    int[] nextFields = new int[3];

                    for(int i=0;i<3;i++){
                        data=scanner.nextLine();
                        nextColors[i]=Integer.parseInt(data);
                    }
                    for(int i=0;i<3;i++){
                        data=scanner.nextLine();
                        nextFields[i]=Integer.parseInt(data);
                    }

                    data=scanner.nextLine();
                    sc=Integer.parseInt(data);

                    //data=scanner.nextLine();
                    //highScore=Integer.parseInt(data);

                    data=scanner.nextLine();
                    if(data.equals("1")){
                        newHighScore=true;
                    } else{
                        newHighScore=false;
                    }

                    data=scanner.nextLine();
                    hours=Integer.parseInt(data);

                    data = scanner.nextLine();
                    minutes=Integer.parseInt(data);

                    data = scanner.nextLine();
                    seconds=Integer.parseInt(data);

                    scanner.close();

                    scanner = new Scanner(new FileReader("RankList/rankList.txt"));
                    data = scanner.nextLine();
                    data = scanner.nextLine();                              // a ranglista első eleme a valaha elért legnagybb pontszámot tartalmazza
                    highScore = Integer.parseInt(data);
                    scanner.close();

                    layout.show(cardPanel,"panelGame");

                    startGame=new StartGame(WINLines.this,score, high_score, time, sc, highScore, hours, minutes, seconds,
                            newHighScore, ecl, frame, layout, cardPanel);               // a játékot a beolvasott adatokkal inicializálom
                    startGame.setFields(fields);
                    startGame.setNextColors(nextColors);
                    startGame.setNextFields(nextFields);
                    thread = new Thread(startGame);
                    thread.start();

                    startGame.setSoundON(soundON);
                    //startGame.run();

                } catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        continueCurrentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                layout.show(cardPanel, "panelGame");
                int sc = startGame.getUpdate().getScore();
                int high_sc = startGame.getUpdate().getHigh_score();
                int hours = startGame.getTimeMeasure().getHours();
                int minutes = startGame.getTimeMeasure().getMinutes();
                int seconds = startGame.getTimeMeasure().getSeconds();
                boolean newHighScore = startGame.getUpdate().getNewHighScore();
                int[] fields = startGame.getFields();
                int[] nextColors = startGame.getNextColors();
                int[] nextFields = startGame.getNextFields();
                int ecl = startGame.getEmpty_cells_left();

                startGame=new StartGame(WINLines.this,score,high_score,time,sc,high_sc,hours,minutes,seconds,newHighScore,
                        ecl, frame, layout, cardPanel);

                startGame.setFields(fields);
                startGame.setNextColors(nextColors);
                startGame.setNextFields(nextFields);
                thread = new Thread(startGame);
                thread.start();

                startGame.setSoundON(soundON);
            }
        });

        panelRankList.add(rankList);
        rankList.setEditable(false);
        rankList.setSize(BOARD_SIZE-50,BOARD_SIZE);
        rankList.setLineWrap(true);
        rankList.setLineWrap(true);

        rankListButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                layout.show(cardPanel,"panelRankList");
                String text="Ranking list:\n";
                try{
                    Scanner scanner = new Scanner(new FileReader("RankList/rankList.txt"));
                    while (scanner.hasNextLine()) {                             // beolvasom a ranglistából az eredményeket, és kiírom őket
                        text = text + scanner.nextLine() + "\n";
                    }
                    scanner.close();
                } catch (Exception ex){
                    ex.printStackTrace();
                }

                rankList.setText(text);

            }
        });

        startGame=new StartGame(WINLines.this,score,high_score,time,0,0,0,0,0,false,
                NR_CELLS*NR_CELLS, frame, layout, cardPanel);           // inicializálom a startGame változót
        //new Thread(startGame).start();

        playSound = new PlaySound("BackgroundMusic.wav",true);
        //playSound.setVolume(0.3);
        soundThread = new Thread(playSound);
        soundThread.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for(int i=0;i<3;i++){
            int[] nextColors = startGame.getNextColors();
            g.drawImage(img[nextColors[i]], (i+(NR_CELLS-3)/2)*CELL_SIZE, 0, null);
        }

        int[] fields=startGame.getFields();

        for (int i = 0; i < NR_CELLS; i++) {
            for (int j = 0; j < NR_CELLS; j++) {
                g.drawImage(img[fields[i*NR_CELLS+j]], j*CELL_SIZE, i*CELL_SIZE+CELL_SIZE+15, null);
            }
        }
    }
}