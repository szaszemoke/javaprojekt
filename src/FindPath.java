import static java.lang.Math.abs;

public class FindPath {
    private int NR_CELLS=9;
    private int csucsok_szama=NR_CELLS*NR_CELLS, kiindulasi_pont,erkezesi_pont;
    private int[] fields = new int[csucsok_szama];
    private int[][] szl = new int[csucsok_szama][5];
    private int[] szomszedok_szama = new int[csucsok_szama];
    private int legr_meg[] = new int[csucsok_szama];
    private int sor[] = new int[csucsok_szama];
    private int tavolsag[] = new int[csucsok_szama];
    private int eredmeny[] = new int[csucsok_szama];
    private int eredmeny_hossz;
    private int e;
    private int u;
    private int el_lehet_jutni;

    public FindPath(int kiindulasi_pont,int erkezesi_pont,int []fields) {
        this.kiindulasi_pont=kiindulasi_pont;
        this.erkezesi_pont=erkezesi_pont;
        this.fields=fields;

        for(int i=0;i<csucsok_szama;i++){
            for(int j=0;j<csucsok_szama;j++){
                if(((i-j==1 && i%NR_CELLS!=0) || (j-i==1 && j%NR_CELLS!=0) || abs(i-j)==NR_CELLS) && (fields[j]==0 || fields[j]>7)){
                    szl[i][szomszedok_szama[i]]=j;          // szomszédsági lista létrehozása
                    szomszedok_szama[i]++;
                }
            }
        }

        for(int i=0;i<csucsok_szama;i++){
            sor[i]=-1;
            tavolsag[i]=-1;
        }

        eredmeny_hossz = 0;
        e = 1;
        u = 1;

        Moore_tavolsag();

        if(tavolsag[erkezesi_pont]==10000 || tavolsag[erkezesi_pont]==-1){
            el_lehet_jutni=-1;
        }else {
            el_lehet_jutni=tavolsag[erkezesi_pont];
            Moore_ut();
        }
    }

    public int getEl_lehet_jutni() {
        return el_lehet_jutni;
    }

    public int[] getEredmeny(){
        return eredmeny;
    }

    void Moore_tavolsag(){
        tavolsag[kiindulasi_pont] = 0;
        for (int v = 0; v < csucsok_szama && v != kiindulasi_pont; v++) {				//kezdeti ertekadas
            tavolsag[v] = -1;
        }
        for(int i=0;i<10;i++){
            sor[i]=0;
        }
        //e++;
        sor[e] = kiindulasi_pont;
        int seged;
        while (e <= u) {
            //Q->x
            seged = sor[e];
            e++;
            for (int i = 0; i < szomszedok_szama[seged]; i++) {					//az elso elem minden szomszedjara
                int szomszed = szl[seged][i];
                if (tavolsag[szomszed] == -1) {
                    legr_meg[szomszed] = seged;
                    tavolsag[szomszed] = tavolsag[seged] + 1;
                    u++;
                    sor[u] = szomszed;
                }
            }
        }
    }

    void Moore_ut(){
        int k = tavolsag[erkezesi_pont];					//milyen tavol van a vegpont ahova erkezunk
        eredmeny[k] = erkezesi_pont;						//ebben tarolom az vegso sort
        while (k != 0) {
            eredmeny[k - 1] = legr_meg[eredmeny[k]];
            k--;
            eredmeny_hossz++;
        }
    }
}
